/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
export default {
    onCreate() {
        console.info("Application onCreate");
    },
    onDestroy() {
        console.info("Application onDestroy");
    }
};
