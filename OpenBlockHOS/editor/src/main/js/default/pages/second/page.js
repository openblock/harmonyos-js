/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
 import router from '@system.router'

import util from '@ohos.util'
export default {
    data: {
        info: 'World'
    },
    log(s) {
        this.info += "\n" + s;
    },
    onclick: function () {
    },
    pageStart:function(e){
        this.log('pageStart');
    },
    pageFinish:function(e){
        this.log('pageFinish');
    },
    pageError:function(e){
        this.log('pageError');
    }
}
