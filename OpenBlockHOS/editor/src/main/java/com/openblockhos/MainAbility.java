package com.openblockhos;

import com.openblockhos.slice.MainAbilitySlice;
import com.openblockhos.slice.RuntimeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.global.configuration.Configuration;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
    }
    @Override
    public void onConfigurationUpdated(Configuration configuration) {
        super.onConfigurationUpdated(configuration);
        // 获取当前slice宽度
//        Utils.setLeftWidth(getContext());
//        // 初始化listContainer数据
//        MainAbilitySlice.initData(Utils.transIdToPixelMap(getContext()));
//        // 重置右侧图片大小
//        RuntimeSlice.setImage(RuntimeSlice.getIdIndex());
    }

    @Override
    protected void onActive() {
//        super.onActive();
    }

    @Override
    protected void onInactive() {
//        super.onInactive();
    }
}
