package com.openblockhos;

import com.openblockhos.slice.RuntimeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.IAbilityContinuation;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.ace.ability.AceAbility;
import ohos.bundle.AbilityInfo;

public class Runtime extends AceAbility{
    @Override
    public void onStart(Intent intent) {
        setInstanceName("default");
        super.onStart(intent);
//        super.setMainRoute(RuntimeSlice.class.getName());
        requestPermissionsFromUser(new String[]{"ohos.permission.DISTRIBUTED_DATASYNC"}, 0);
    }

    @Override
    public void onBackground() {
        terminateAbility();
    }
}
